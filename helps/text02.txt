Prüf Handynummer

1) SQL: Geh auf Mobil Statistik, um die SQL Abfragen zu entnehmen.
2) Actions:
  a) "Netz-Button" - Anhand der Vorwahl wird das Funknetz ermittelt.
  b) "Mobile-Button" - Diese Aktion überprüft die Anzahl der Zahlen der Handynummer, Vorwahl & Netz.
  c) "aktiv_status-Button" - Anhand des "netz_status" wird aktiv_status auf "erfolgreich" oder "delete" geändert.
