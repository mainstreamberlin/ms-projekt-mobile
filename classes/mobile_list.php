<?php
class MsMobileList extends WP_List_Table {

  public $dbtable = 'wiml_mobile';
  public $hiddenColumns = 'managetoplevel_page_ath-mobilecolumnshidden';
  public function __construct()
  {
    //Set parent defaults
    parent::__construct( array(
        'singular'  => 'mobile',     //singular name of the listed records
        'plural'    => 'mobiles',    //plural name of the listed records
        'ajax'      => true        //does this table support ajax?
    ));
  }
  /*
		fn
	*/
  public function get_cols(){
		global $wpdb;
		return $wpdb->get_col( "DESC " . $this->dbtable, 0 );
	}
  /*
		fn
	*/
	public function sql( $per_page = 10, $page_number = 1  )
	{
		$sql = "SELECT * FROM ". $this->dbtable ." ";
		$sql .= "WHERE id!='' AND " .$this->deactived();

		/* SEARCH */
		if ( ! empty( $_REQUEST['s'] ) ) {
				$sql .= " AND ( mobile LIKE '%".$_REQUEST["s"]."%' OR name LIKE '%".$_REQUEST["s"]."%' OR uid LIKE '%".$_REQUEST["s"]."%' )";
		}

		/* Category */
		if ( ! empty( $_REQUEST['cat'] ) ) {
				$sql .= " AND club='".$_REQUEST["cat"]."'";
		}

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		} else {
			$sql .= ' ORDER BY id desc ';
		}

		$sql .= " LIMIT ".$per_page;
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
    //var_dump( "<br>" .$sql );
		return $sql;
	}
  /*
    fn
  */
  public function deactived($alias="")
  {
    $alias = ($alias) ? $alias . '.' : '';
    return $alias . "blacklist=0 AND " . $alias . "aktiv_status!='deleted'";
  }
  /*
    fn
  */
  public function table_data( $per_page = 10, $page_number = 1 )
  {
    global $wpdb;

    $sql       = $this->sql( $per_page, $page_number );
    $data      = $wpdb->get_results($sql, ARRAY_A);

    return $data;
  }
  /*
    fn
  */
  public function record_count()
  {
    global $wpdb;
    $sql  =  substr($this->sql(), 0, strpos($this->sql(), 'ORDER')-1);
    $sql  = str_replace('*','COUNT(*)', $sql);
    $data = $wpdb->get_var($sql);

    return $data;
  }
  /*
    fn
  */
  public function get_sortable_columns()
  {
    $sortable_columns = array();
    $cols = $this->get_cols();

    foreach( $cols as $col ){
      $sortable_columns[ $col ] = array( $col, true );
    }

    return $sortable_columns;
  }
  /*
    fn
  */
  public function column_default( $item, $column_name )
  {
    return $item[ $column_name ];
  }
  /*
    fn
  */
  function get_columns()
  {
    $column_name = array();
    $cols = $this->get_cols();

    foreach( $cols as $col ){
      $column_name[ $col ] = $col;
    }

    return $column_name;
  }
  /*
		fn
	*/
	public function get_hidden_columns()
  {
		$columns =  (array) get_user_option( $this->hiddenColumns );
    //var_dump( $columns );
		if ( count( $columns )  < 1 ){
			$columns = array('name', 'ip', 'herkunft', 'herkunftsdatum', 'importdatum', 'blacklist_datum', 'netz_statusdatum');
		}
    return $columns;
  }
  /*
		fn
	*/
	public function column_id($item){
    $page    = '';
    $paged   = '';
    $orderby = '';
    $order   = '';
    $s       = '';
    $cat     = '';
    extract( $_REQUEST );
		$actions = array(
			'edit'  => sprintf('<a href="?page=%s&action=%s&id=%s&paged=%d&orderby=%s&order=%s&s=%s&cat=%s">Edit</a>',
									$page,
									'edit',
									$item['id'],
									$paged,
									$orderby,
									$order,
									$s,
									$cat),
		);
		return sprintf('%1$s %2$s',$item['id'],$this->row_actions($actions));
	}
  /*
    fn
  */
  public function get_table_classes() {
    return array( 'widefat', '', 'striped', $this->_args['plural'] );
  }
  /*
    fn
  */
  public function prepare_items()
  {
    /** Process bulk action */
    $this->process_bulk_action();

    $user			      = get_current_user_id();
    $screen			    = get_current_screen();
    $screen_option	= $screen->get_option('per_page', 'option');
    $per_page		    = get_user_meta($user, $screen_option, true);
    $current_page	  = $this->get_pagenum();
    $total_items 	  = $this->record_count();

    if ( empty ( $per_page ) || $per_page < 1 ) {
      $per_page	= $screen->get_option( 'per_page', 'default' );
    }

    $this->set_pagination_args( array(
      'total_items' => $total_items, //WE have to calculate the total number of items
      'per_page'    => $per_page //WE have to determine how many items to show on a page
    ) );

    $columns  = $this->get_columns();
    $hidden   = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array($columns, $hidden, $sortable);
    $this->items = $this->table_data( $per_page, $current_page );

  }
  /*
    fn
  */
  public function extra_tablenav( $which )
  {
    global $wpdb;
    if ( $which == "top" ){
      $where	= 'WHERE ' . $this->deactived();
      $sql	= "select club, count(DISTINCT mobile) as 'anz' from ".$this->dbtable." ".$where." GROUP BY club";
      $clubs = $wpdb->get_results( $sql );

      if ( COUNT($clubs) < 1 ) return '';
      echo '<select name="cat">';
      echo '<option value="">' . __("All") . ' Clubs</option>';
      foreach( $clubs as $club ){
        echo '<option value="'. $club->club .'" '.( ($_REQUEST["cat"] == $club->club) ? 'selected' : '').'>' . $club->club . ' ('. $club->anz .') </option>';
      }
      echo '</select>';
      echo '<input type="submit" name="senden" value="'. __("Submit") .'" class="button">';
    }
  }
}
?>
