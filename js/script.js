jQuery(document).ready(function($){
  var speed = 100;
  $("body")
    .on('submit', '.msmobile-form-mobile', function(e){
  		e.preventDefault();
  		var mobile_form_user = $(this);
  		var postbox_mobile_form_user = mobile_form_user.parents('.postbox');

  		$.ajax({
  				url: mobile_form_user.attr("action"),
  				data: mobile_form_user.serializeArray(),
  				dataType: 'jsonp',
  				type:'post',
  				beforeSend: function(formData, jqForm, options) {
  					$(".notice").remove();
  				},
  				complete : function(jqXHR, textStatus) {
  					var response = $.parseJSON( jqXHR.responseText );
  					var msg = '<div class="notice notice-'+response.status+'"><p>'+response.statustext+'</p></div>';
  					postbox_mobile_form_user.find('.inside').load(  postbox_mobile_form_user.find('.inside').data("href") );
  					$('#dashboard-widgets').before( msg );
  					mobile_form_user.find(".submit").append( msg );
  				},
  		});
  		return false;
  	})
		.on( "click", "#postbox-mobile-actions a", function(){
      var columnClass = '.' + $(this).data("column");
      var mobileClass = ".mobile";
      var tableDataSelector = '#postbox-mobile-table tbody tr';
      var count = 1;
      var gesamt = $( tableDataSelector ).length;
      /*Progressbar*/
      addProgressbar();

      $( tableDataSelector ).each( function (index ){
          var $this = $(this);

          setTimeout( function(){

            $.ajax({
      				url: $this.find(columnClass).data("href"),
      				data: "",
      				dataType: 'json',
      				type:'get',
              success : function(response) {
                /*console.log( response );*/
                $this.find( columnClass ).html( response.statustext );
      				}
      			});
            var percentage = Math.ceil(count / gesamt * 100);
            var args = {};
            args['count'] = count;
            args['gesamt'] = gesamt;
            args['percentage'] = percentage;
            animateProgressbar(args);
            $this.addClass("selected");
            count++;

          }, index*speed);
      });
      return false;
    })
    .on( "click", "#showUploadedFileData-Saved", function(){
      var columnClass = '.col-0';
      var mobileClass = ".mobile";
      var tableDataSelector = '#postbox-mobile-report-table tr';
      var count = 1;
      var gesamt = $( tableDataSelector ).length;

      /*Progressbar*/
      $("body").append('<div class="ms-results"></div>');
      addProgressbar();

      $( tableDataSelector ).each( function (index ){
        var $this = $(this);
        setTimeout( function(){
          if ($this.find(columnClass).data("href")){
              $.ajax({
                url: $this.find(columnClass).data("href") + '&netz_status='+ $this.find('.col-7').text() + '&netz_statusdatum=' + $this.find('.col-6').text(),
                data: "",
                dataType: 'json',
                type:'get',
                success : function(response) {
                  /*console.log( response );*/
                  $this.find( '.result' ).html( response.statustext );
                }
              });
              /*Progressbar*/
              var percentage = Math.ceil(count / gesamt * 100);
              var args = {};
              args['count'] = count;
              args['gesamt'] = gesamt;
              args['percentage'] = percentage;
              animateProgressbar(args);
              $this.addClass("selected");
              count++;
          }
        }, index*speed);
      });
      return false;
    });
});
