<?php
/**
 * Stats
 */
class MSMobileStats
{
  public $dbtable = 'wiml_mobile';
  public $anzBlacklist;
  public $anzDeleted;
  public $usedData;
  public function __construct()
  {
    # code...
  }
  public function display(){
    $args = array(
    "postbox_class" => array("first" => "col-5 first", "second" => "col-5 last")
    );

    $widgets = array(
    array("align" => "first", "title" => "Allgemein", "data" => $this->stats_mobile() ),
    array("align" => "first", "title" => "Neue MobileNr", "data" => $this->YearsActive() ),
    array("align" => "first", "title" => "aktiv_status", "data" => $this->stat_aktiv_status() ),
    array("align" => "first", "title" => "Netz", "data" => $this->stat_netz() ),

    array("align" => "second", "title" => "netz_status", "data" => $this->stat_netz_status()),
    array("align" => "second", "title" => "netz_status - Erfolgreich (TRANSMITTED)", "data" => $this->stat_netz_status_erfolgreich("TRANSMITTED")),
    array("align" => "second", "title" => "netz_status - Erfolgreich (DELIVERED)", "data" => $this->stat_netz_status_erfolgreich("DELIVERED") ),
    array("align" => "second", "title" => "netz_status (Leer)", "data" => $this->stat_netz_status_empty() )
    );

    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  public function stats_mobile(){
    global $wpdb;
    $out .= '<table class="wp-list-table widefat striped">';

    $sql = "SELECT COUNT(DISTINCT mobile) FROM ".$this->dbtable ." WHERE blacklist=0 AND aktiv_status!='deleted'";
    $out .= '<tr><td class="highlight"><span class="dashicons dashicons-warning" title="'.$sql.'"></span> Benutzte Daten</td><td class="highlight">' . $gesamt = $wpdb->get_var($sql) .'</td></tr>';

    $sql ="SELECT COUNT(DISTINCT mobile) FROM ".$this->dbtable ." WHERE blacklist=1";
    $out .= '<tr><td><span class="dashicons dashicons-warning" title="'.$sql.'"></span> Blacklist</td><td>' . $blacklist = $wpdb->get_var($sql) .'</td></tr>';

    $sql = "SELECT COUNT(DISTINCT mobile) FROM ".$this->dbtable ." WHERE blacklist=0 AND aktiv_status='deleted'";
    $out .= '<tr><td><span class="dashicons dashicons-warning" title="'.$sql.'"></span> Delete</td><td>' . $deleted = $wpdb->get_var($sql) .'</td></tr>';

    $sql = "SELECT COUNT(DISTINCT mobile) FROM ".$this->dbtable;
    $out .= '<tr><td><span class="dashicons dashicons-warning" title="DB: '.$sql.'"></span> Gesamt</td><td>PHP: '.($gesamt+$blacklist+$deleted).' <br> &nbsp DB: ' . $wpdb->get_var($sql) .'</td></tr>';
    $out .= '</table>';

    $this->anzBlacklist = $blacklist;
    $this->anzDeleted = $deleted;
    $this->usedData = $gesamt;

    return $out;
  }
  public function stat_aktiv_status(){
    global $wpdb;

    $out .= '<table class="wp-list-table widefat striped">';
    $sql = 'SELECT aktiv_status, count(aktiv_status) as anz FROM '.$this->dbtable.' WHERE blacklist = 0 group by aktiv_status';
		$res = $wpdb->get_results($sql);
    $sendSql = 'SELECT * FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status=';
    $gesamt = 0;
    $out .= '<tr><td colspan="3">'.$sql.'</td></tr>';
		foreach ($res as $r){
		      $out .= '<tr><td>'.$r->aktiv_status.'</td><td>'.$r->anz.'</td><td align="right"><a href="'.admin_url("admin.php").'?page=ms-mobile-check&data_show=true&sql='.urlencode($sendSql.'"'.$r->aktiv_status.'"').'" class="button">'.__("Show Details").'</a></td></tr>';
          $gesamt += $r->anz;
    }
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td align="right">mit Blacklist: '.($gesamt+$this->anzBlacklist).'</td></tr>';
    $out .= '</table>';

    return $out;
  }
  /*
    fn stat_netz_status
  */
  public function stat_netz_status(){
    global $wpdb;
    $sql 	= 'SELECT netz_status, count(netz_status) as anz FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status!="deleted" group by netz_status';
		$res 	= $wpdb->get_results($sql);
		$gesamt = 0;
    $sendSql = 'SELECT * FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status!="deleted" and netz_status=';

    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr><td colspan="4">'.$sql.'</td></tr>';
		foreach ($res as $r){
			$gesamt = $gesamt + $r->anz;
      $out .= '<tr>';
      $out .= '<td>'.$r->netz_status.'</td><td>'.( ($r->anz > 0) ? $r->anz : 0 ).'</td>';
      $out .= '<td><a target="_blank" href="'. admin_url("admin.php") .'?page=ms-mobile-check&data_show=true&sql='. rawurlencode( $sendSql.'"'.$r->netz_status.'"' ) .'" class="button">'.__("Show Details").'</a>';
      $out .= '<td align="right"><a target="_blank" href="'. admin_url("admin-ajax.php") .'?action=mobile_stats_export&filename='.$r->netz_status.'&sql='. rawurlencode( $sendSql.'"'.$r->netz_status.'"' ) .'" class="button">'.__("Export").'</a>';
      $out .= '</tr>';
		}
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td align="right" colspan="2">(Blacklist+Deleted): '.($gesamt+$this->anzBlacklist+$this->anzDeleted).'</td></tr>';
		$out .= '</table>';
    return $out;
  }
  /*
    fn
  */
  public function stat_netz_status_empty(){
    global $wpdb;
    $sql 	= 'SELECT club, count(club) as anz FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status!="deleted" AND netz_status="" group by club';
		$res 	= $wpdb->get_results($sql);
		$gesamt = 0;
    $sendSql = 'SELECT * FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status!="deleted" AND netz_status="" AND club=';

    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr><td colspan="3">'.$sql.'</td></tr>';
		foreach ($res as $r){
			$gesamt = $gesamt + $r->anz;
      $out .= '<tr>';
      $out .= '<td>'.$r->club.'</td><td>'.( ($r->anz > 0) ? $r->anz : 0 ).'</td>';
      $out .= '<td align="right"><a target="_blank" href="'. admin_url("admin.php") .'?page=ms-mobile-check&data_show=true&sql='. rawurlencode( $sendSql.'"'.$r->club.'"' ) .'" class="button">'.__("Show Details").'</a>';
      $out .= '</tr>';
		}
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td align="right"></td></tr>';
		$out .= '</table>';
    return $out;
  }
  /*
    fn
  */
  public function stat_netz_status_erfolgreich( $netz_status='TRANSMITTED' ){
    global $wpdb;
    $sql 	= 'SELECT club, count(club) as anz FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status!="deleted" AND netz_status="'.$netz_status.'" group by club';
		$res 	= $wpdb->get_results($sql);
		$gesamt = 0;
    $sendSql = 'SELECT * FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status!="deleted" AND netz_status="'.$netz_status.'" AND club=';

    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr><td colspan="4">'.$sql.'</td></tr>';
		foreach ($res as $r){
			$gesamt = $gesamt + $r->anz;
      $out .= '<tr>';
      $out .= '<td>'.$r->club.'</td><td>'.( ($r->anz > 0) ? $r->anz : 0 ).'</td>';
      $out .= '<td align="right"><a target="_blank" href="'. admin_url("admin.php") .'?page=ms-mobile-check&data_show=true&sql='. rawurlencode( $sendSql.'"'.$r->club.'"' ) .'" class="button">'.__("Show Details").'</a>';
      $out .= '<td align="right"><a target="_blank" href="'. admin_url("admin-ajax.php") .'?action=mobile_stats_export&filename='.$r->club.'&sql='. rawurlencode( $sendSql.'"'.$r->club.'"' ) .'" class="button">'.__("Export").'</a>';
      $out .= '</tr>';
		}
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td align="right" colspan="2"></td></tr>';
		$out .= '</table>';
    return $out;
  }
  /*
    fn
  */
  public function stat_netz(){
    global $wpdb;
  	$sql 	= 'SELECT netz, count(netz) as anz FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status != "deleted" group by netz';
		$res 	= $wpdb->get_results($sql);
		$data["netz_staus_ohne_aktiv_status"]["sql"] = $sql;
		$gesamt = 0;
    $sendSql = 'SELECT * FROM '.$this->dbtable.' WHERE blacklist = 0 and aktiv_status != "deleted" AND netz=';

    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '<tr><td colspan="3">'.$sql.'</td></tr>';
		foreach ($res as $r){
			$gesamt = $gesamt + $r->anz;
      $out .= '<tr>';
      $out .= '<td>'.$r->netz.'</td><td>'.( ($r->anz > 0) ? $r->anz : 0 ).'</td>';
      $out .= '<td><a target="_blank" href="'. admin_url("admin.php") .'?page=ms-mobile-check&data_show=true&sql='. rawurlencode( $sendSql.'"'.$r->netz.'"' ) .'" class="button">'.__("Show Details").'</a>';
      $out .= '</tr>';
		}
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td align="right">(Blacklist+Deleted): '.($gesamt+$this->anzBlacklist+$this->anzDeleted).'</td></tr>';
		$out .= '</table>';
    return $out;
  }
  /*
    fn
  */
  public function YearsActive()
  {
    global $wpdb;
    $gesamt = 0;
    /*58184*/
    $sql = "SELECT COUNT(*) as anz, YEAR(herkunftsdatum) as hd FROM " . $this->dbtable ." WHERE YEAR(herkunftsdatum)!='0' AND blacklist = 0 and aktiv_status != 'deleted' GROUP BY hd ORDER BY hd desc";
    /*var_dump($sql);*/
    $users = $wpdb->get_results( $sql );

    $args = array(
      "maxHeight" => 60,
      "unit"      => 100,
      "barWidth"  => 40,
      "x" => array("title" => "Monat", "col" => "hd"),
      "y" => array("title" => "Anzahl", "col" => "anz"),
      "data" => $users);

    $MSCharts = new MSCharts( $args );
    $out = $MSCharts->bar();

    $out .= '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ) :
      $out .= '<tr>';
      $out .= '<td>'. $user->hd .'</td>';
      $out .= '<td>'. $user->anz .'</td>';
      $out .= '<td>'. $percent = round(($user->anz/$this->usedData*100)) .'%</td>';
      $out .= '</tr>';
      $gesamt += $user->anz;
      $gesamtPercent += $percent;
    endforeach;
    $out .= '<tr><td class="highlight">Gesamt</td><td class="highlight">'.$gesamt.'</td><td class="highlight">'.$gesamtPercent.'%</td></tr>';
    $out .= '<tr><td>Ohne Datum</td><td>'.($this->usedData-$gesamt).'</td><td>'.(100-$gesamtPercent).'%</td></tr>';
    $out .= '</table>';

    return $out;
  }
}

?>
