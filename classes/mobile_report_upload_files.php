<?php
/**
 *
 */
class MSMobileReportUploadFiles
{
  public $folder = 'sms-report/';
  public $pfad;
  public $file;
  public $dbtable = 'wiml_mobile';
  public $stats;

  public function __construct()
  {
    $upload_dir = wp_upload_dir();
    $this->pfad = $upload_dir['basedir'] .'/'. $this->folder;

    $this->uploadFile();
    $this->file = $_REQUEST["file"];
  }
  public function form()
  {
  ?>
    <table width="100%">
    <tr>
      <td valign="top">
        <form action="<?php echo admin_url("admin.php") ?>?page=<?php echo $_REQUEST["page"] ?>" method="post" enctype="multipart/form-data">
        <p><label for="file">Filename:</label>
        <input type="file" name="file" id="file" />
        <input type="hidden" name="page" value="<?php echo $_REQUEST["page"] ?>" /> </p>
        <p><input type="submit" name="submit" value="Datei hochladen" class="button button-primary" /></p>
        </form>
      </td>
    </tr>
    </table>
  <?php
  }
  public function display()
  {
    $showUploadedFileData = $this->showUploadedFileData();
  ?>
    <div id="dashboard-widgets" class="metabox-holder">
      <div id="postbox-mobile-report-upload-form" class="postbox-container">
          <div class="meta-box-sortables ui-sortable">
              <div class="postbox">
                  <h2 class="hndle ui-sortable-handle">CSV hochladen</h2>
                  <div class="inside" data-href=""><?php $this->form() ?></div>
              </div>
              <div class="postbox">
                  <h2 class="hndle ui-sortable-handle"><?php printf( __('%s files'), 'Report');  ?></h2>
                  <div class="inside" data-href=""><?php echo $this->showUploadedFiles(); ?></div>
              </div>
              <div class="postbox">
                  <h2 class="hndle ui-sortable-handle"><?php printf( __('%s files'), 'Report');  ?></h2>
                  <div class="inside" data-href=""><?php echo $this->stats(); ?></div>
              </div>
          </div>
      </div>
      <div id="postbox-mobile-report-table" class="alignleft">
          <div class="meta-box-sortables ui-sortable">
              <div class="postbox">
                  <h2 class="hndle ui-sortable-handle">Report-Datei: <?php echo $this->file ?> <a href="#" id="showUploadedFileData-Saved" class="button button-primary handlediv" style="width:auto;"><?php _e("Take over")?></a></h2>
                  <div class="inside" data-href=""><?php echo $showUploadedFileData ?></div>
              </div>
          </div>
      </div>
    </div>
  <?php
  }
  public function uploadFile(){
    if ($_POST["submit"]) {
      echo '<div class="updated">';
  		if ($_FILES["file"]["error"] > 0)
  		{
  			echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
  		}
  		else
  		{
  				echo "Upload: " . $_FILES["file"]["name"] . "<br />";
  				echo "Type: " . $_FILES["file"]["type"] . "<br />";

  				$pfad = $upload_dir['basedir'] .'/'. $this->pfad;
  				if (file_exists($pfad . $_FILES["file"]["name"]))
  				{
  				  echo $_FILES["file"]["name"] . " -  Existierte Datei wurde gelöscht und erneuert.<br />";
  				  chmod($pfad . $_FILES["file"]["name"], 0777);
  				  unlink($pfad . $_FILES["file"]["name"]);
  				}

  				move_uploaded_file($_FILES["file"]["tmp_name"], $pfad . $_FILES["file"]["name"]);
  				chmod($pfad . $_FILES["file"]["name"], 0777);

  				echo '</div>';
  				return $_FILES["file"]["name"];
  		}
  		echo '</div>';
    }
  }
  public function showUploadedFiles(){

		$pfad = $this->pfad;
		$handle=opendir ($pfad);

		while ($datei = readdir ($handle)) {
			if(strlen($datei) > 2){
				$pfadDatei = $pfad.$datei;
				$files[filemtime($pfadDatei)] = $datei;
			}
		}
		closedir($handle);
		$datei = "";

		krsort($files);

    $out = '<table class="wp-list-table widefat striped">';
    $out .= '<tr>';
    $out .= '<th>Datei</th>';
    $out .= '<th>Anzahl</th>';
    $out .= '<th></th>';
    $out .= '</tr>';

    foreach($files as $datei) {
			if(strlen($datei) > 2){
				$pfadDatei = $pfad.$datei;
				$zeilen = file( $pfad.$datei );
				$anz = count($zeilen)-1;

        $out .= '<tr>';
        $out .= '<td><strong>' . $datei . '</strong><br>'.  ((strlen($datei) > 2) ? date("d.m.Y - H:i:s", filemtime($pfadDatei)) : '') .'</td>';
        $out .= '<td>' . $anz . '</td>';
        $out .= '<td><a href="?page=' . $_REQUEST["page"]. '&file='. $datei .'" class="button">'.__("Show Details").'</a></td>';
        $out .= '</tr>';
			}
		}
    $out .= '</table>';
		return $out;
	}
  public function stats(){
    $netz_status = $this->stats;
    $count_netz_status = array_count_values($netz_status);
    $gesamt = 0;

    $out = '<table class="wp-list-table widefat striped">';
    foreach( $count_netz_status as $nsk => $nsv){
      if($nsk){
        $out .= '<tr>';
        $out .= '<td>'.$nsk.'</td><td>'.$nsv.'</td>';
        $out .= '</tr>';
        $gesamt += $nsv;
      }
    }
    $out .= '<tr>';
    $out .= '<td>Gesamt</td><td>'.$gesamt.'</td>';
    $out .= '</tr>';
    $out .= '</table>';
    return $out;
  }
  public function showUploadedFileData(){
    if( !$this->file )
      return __("No such file exists! Double check the name and try again.");

    $datei = $this->pfad . $this->file;
		if (file_exists( $datei )){

			$byte = fopen($datei, "r");
			$tech = array();
			$anzahl_zeile = 0;
			$i = 1;
			$text = "";

      $out = '<table class="wp-list-table widefat striped mobile-report-upload-file">';
			while ($data = fgetcsv ($byte, 1000, ";"))
			{
        $out .= '<tr>';
				$content = $data;
				$anzahl_spalte = 0;
				foreach($content as $ta){
						$sp[$anzahl_zeile][$anzahl_spalte] =  $ta;
            if( $anzahl_spalte == 0 && $anzahl_zeile > 0)
            {
              $out .= '<td data-href="'.admin_url("admin-ajax.php").'?action=mobile_report_check_aktiv_status&mobile='.$ta.'" class="row-'.$anzahl_zeile.' col-'.$anzahl_spalte.'">' . utf8_encode($ta) . '</td>';
            } else {
              $stats[] = $this->showUploadedFileDataStats( array("column" => $anzahl_spalte, "row" =>$anzahl_zeile, "data" => $ta) );
              $out .= '<td class="row-'.$anzahl_zeile.' col-'.$anzahl_spalte.'">' . utf8_encode($ta) . '</td>';
            }
						$anzahl_spalte++;
				}
        $out .= '<td class="result"></td>';
        $out .= '</tr>';
				$anzahl_zeile++;
			}
      $out .= '</table>';
      $this->stats = $stats;
			return $out;
		}
	}
  public function showUploadedFileDataStats($args){
    $col = $args["column"];
    $row = $args["row"];
    $data = $args["data"];
    if($col == 7 && $row > 0){
      return $data;
    }
    return '';
  }
  public function ajax_netz_status(){
    global $wpdb;
    $MSMobileReportUploadFiles = new MSMobileReportUploadFiles;
    $mobile = $_REQUEST["mobile"];
    $mobile = MSMobileCheckActions::checkVorwahlMobileNr($mobile);

    $netz_status = $_REQUEST["netz_status"];
    $netz_statusdatum = $_REQUEST["netz_statusdatum"];
    $netz_statusdatum = date_i18n("Y-m-d H:i:s", strtotime($netz_statusdatum));
    $status = array(
      'DELIVERED'     => 'erfolgreich',
      'TRANSMITTED'   => 'erfolgreich',
      'ACCEPTED'   => 'erfolgreich',
      'BUFFERED' => 'deleted',
      'NOT_DELIVERED' => 'deleted',
      '-' => 'deleted',

      'Die Nachricht wurde versendet.'  => 'erfolgreich',
      'Keine weiteren Auslieferungsversuche, Empfänger unbekannt oder gesperrt.'  => 'deleted',
      'Nachricht wurde nicht ausgeliefert, Empfänger unbekannt.'  => 'deleted'
    );

    if ( $mobile && $netz_status){
      $check = $wpdb->get_row("SELECT mobile FROM " . $MSMobileReportUploadFiles->dbtable ." WHERE mobile='".$mobile."'");
      if( $check->mobile )
      {
        $update_netz_status = '';
        if (array_key_exists($netz_status, $status)) {
            $update_netz_status = ", aktiv_status='".$status[$_REQUEST["netz_status"]]."', updatetime='".current_time("Y-m-d H:i:s")."' ";
        }
        $sql ="UPDATE " . $MSMobileReportUploadFiles->dbtable ." SET netz_status='".$netz_status."', netz_statusdatum='".$netz_statusdatum."' ".$update_netz_status." WHERE mobile='". $check->mobile ."'";
        $wpdb->query($sql);
        wp_send_json( array("statustext" => $mobile . ' | ' .$netz_status ) );
      }
      else {
        wp_send_json( array("statustext" => $mobile .' existiert nicht!') );
      }
    } else{
      wp_send_json( array("statustext" => false) );
    }
    wp_die();
  }
}
?>
