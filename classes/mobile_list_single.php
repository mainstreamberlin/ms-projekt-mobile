<?php
/**
 * Mobile single
 */
class MsMobileListSingle
{
  public $id;
  public $cols;
  public $dbtable = 'wiml_mobile';
  /*
    fn
  */
  public function __construct()
  {
      if ( !$_REQUEST["id"] )
			   wp_die( __('You attempted to edit an item that doesn&#8217;t exist. Perhaps it was deleted?') );

      global $wpdb;
      $this->id = $_REQUEST["id"];
      $this->cols = $wpdb->get_col( "DESC " . $this->dbtable );
  }
  /*
		fn
	*/
	public function user_sql()
	{
		global $wpdb;

		$sql = "SELECT * FROM ".$this->dbtable." WHERE id='". $this->id ."'";
		return $wpdb->get_row( $sql );
	}
  /*
    fn
  */
  public function deactived($alias="")
  {
    $alias = ($alias) ? $alias . '.' : '';
    return $alias . "blacklist=0 AND " . $alias . "aktiv_status!='deleted'";
  }
  /*
		fn
	*/
	public function info()
	{
		global $wpdb;
		$user = $this->user_sql();
		?>
        <form action="<?php echo admin_url("admin-ajax.php") ?>" class="msmobile-form-mobile" method="post">
        <input type="hidden" name="action" value="mobile_update_user">
        <input type="hidden" name="id" value="<?php echo $user->id ?>">
        <table class="widefat striped">
		<?php
		foreach( $this->cols as $col )
		{
			$type = '';
			switch( $col ){
				case 'id' :
					$args = array('type' => 'text', 'name' => $col, 'value' => $user->$col, 'disable' => true );
					break;
				case 'club' :
        case 'cat' :
        case 'netz_status' :
        case 'netz' :
            $options = array();
					  $sql	= "SELECT ".$col." FROM ".$this->dbtable." WHERE ".$this->deactived()." GROUP BY ".$col." ORDER BY ".$col." ASC";
					  $clubs = $wpdb->get_results($sql);
					  foreach( $clubs as $club ) $options[ $club->$col ] = $club->$col;
					  $args = array('type' => 'select', 'name' => $col, 'value' => $user->$col, 'options' => $options, 'class' => '' );
					  break;
      case 'aktiv_status' :
          $options = array();
  			  $sql	= "SELECT ".$col." FROM ".$this->dbtable." GROUP BY ".$col." ORDER BY ".$col." ASC";
  			  $clubs = $wpdb->get_results($sql);
  			  foreach( $clubs as $club ) $options[ $club->$col ] = $club->$col;
  			  $args = array('type' => 'select', 'name' => $col, 'value' => $user->$col, 'options' => $options, 'class' => '' );
  			  break;
				case 'blacklist' :
					$args = array('type' => 'checkbox', 'name' => $col, 'value' => $user->$col );
					break;

				case 'uid' :
						$args = array('type' => 'text', 'name' => $col, 'value' => $user->$col, 'icon' => array('dashicons-admin-users', 'User ansehen', admin_url("admin.php").'?page=ath-users&action=edit&uid=' .$user->$col ) );
						break;

				default :
					$args = array('type' => 'text', 'name' => $col, 'value' => $user->$col );
					break;
			}
			?>
      <tr class="<?php echo $col ?>"><td><?php echo $col ?></td><td><?php echo $this->input( $args ); ?></td></tr>
      <?php
		}
		?>
      <tr><td></td><td><?php submit_button( __("Save") );?></td></tr>
      </table>
		</form>
		<?php
	}
  /*
		fn
	*/
  public function display()
  {
  ?>
      <div id="dashboard-widgets" class="metabox-holder">
        <p class=""><a href="<?php echo admin_url('admin.php') . '?' . str_replace(array('&action=edit', '&id=' .$this->id), '', $_SERVER['QUERY_STRING']) ?>"><?php _e("Go back") ?></a></p>
        <div id="postbox-user-userinfo" class="alignleft">
            <div class="meta-box-sortables ui-sortable">
                <div class="postbox">
                    <h2 class="hndle ui-sortable-handle">Mobile</h2>
                    <div class="inside" data-href="<?php echo admin_url('admin-ajax.php') ?>?action=mobile_userinfo&id=<?php echo $this->id ?>"><?php $this->info() ?></div>
                </div>
            </div>
        </div>
      </div>
  <?php
  }
  /*
		fn
	*/
	public function input( $args=array() )
	{
		$disable  = '';
    $icon     = '';
    $args     = is_array( $args )  ? $args : array();

		if(@$args["disable"] == true){
			$disable = 'disabled';
		}
		if( @is_array( $args["icon"] ) )
		{
			$icon = ' <a href="'.(($args["icon"][2]) ? $args["icon"][2] : '#').'" title="'. $args["icon"][1] .'"><span class="dashicons '.$args["icon"][0].'"></span></a>';
		}

		if($args["type"] == "checkbox"){
			return '<input type="checkbox" name="'.@$args["name"].'" value="1"class="'.@$args["class"].'" '.((@$args["value"] == 1) ? 'checked' : '').'>' . $icon;
		}
		if($args["type"] == "select"){
			$out = '';
			$out .= '<select name="'.$args["name"].'" class="'.$args["class"].'">';
      $out .= '<option></option>';
			foreach( $args["options"] as $optk => $optv ){
				$out .= '<option value="'.$optv.'" '. ( ($optv == $args["value"] ) ? 'selected' : '' ) .'>' . $optv . '</option>';
			}
			$out .= '</select>' . $icon;
			return $out;
		}

		return '<input type="text" name="'.@$args["name"].'" value="'.@$args["value"].'" class="'.@$args["class"].'" style="width:'.((@$args["icon"]) ? '85%' : '100%').'" '.$disable.'>'. $icon;
	}
  /*
		fn
	*/
	public function ajax_update_user(){
		global $wpdb;
		$MsMobileListSingle = new MsMobileListSingle;

		if( !$MsMobileListSingle->uid && !$_REQUEST["mobile"]) return '';

		$table		= $MsMobileListSingle->dbtable;
		$data			= array();
		$cols			= array_diff( $MsMobileListSingle->cols ,array("id") );

		foreach($cols as $col ) $data[ $col ] = $_REQUEST[ $col ];

		/* NEW DATA */
		$data["updatetime"]	=	current_time("Y-m-d H:i:s");

		$format			= '%s';

		$where			= array('id' => $MsMobileListSingle->id );
		$whereformat	= '%d';

		$result = $wpdb->update( $table, $data, $where, $format, $where_format );

		if( $result ){
			$response['status'] = 'success';
			$response['statustext'] = __("Changes saved.");

		}else {
			$response['status'] = 'error';
			$response['statustext'] =  __("An unidentified error has occurred.");

		}
		wp_send_json( $response );
		wp_die();
	}
  public function ajax_userinfo(){
		$MsMobileListSingle = new MsMobileListSingle;
		$MsMobileListSingle->info();
		wp_die();
	}
}

?>
